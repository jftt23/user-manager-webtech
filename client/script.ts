class User {
    public id?: string;
    public firstname: string;
    public lastname: string;

    constructor(firstName: string, lastName: string, id?: string) {
        this.firstname = firstName;
        this.lastname = lastName;
        this.id = id;
    }
}

const url: String = "http://localhost:5000/v1"

let userList: User[] = [];

function initUsers() {
    $.ajax({
        url: `${url}/users`, 
        type: "GET", 
        success: (success, status, jqXHR) => {
            const newUsers = success as User[]
            userList = newUsers;
            renderUserList();
        },
        dataType: "json"
    })
}

function addUser(event: any) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();
    console.log("hallo hier bin ich")
    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const firstNameField: JQuery = $('#add-first-name-input');
    const lastNameField: JQuery = $('#add-last-name-input');

    // Read values from input fields
    const firstName: string = firstNameField.val().toString().trim();
    const lastName: string = lastNameField.val().toString().trim();

    // Check if all required fields are filled in
    if (firstName && lastName) {
        // Create new user
        let newUser = new User(firstName, lastName);
        $.ajax({
            url: `${url}/users`, 
            type: "POST", 
            data: JSON.stringify({lastname: newUser.lastname, firstname: newUser.firstname}),
            success: (success, status, jqXHR) => {
                newUser = success as User
                userList.push(newUser);
                renderMessage('User created');
                addUserForm.trigger('reset');
                renderUserList();
            },
            contentType: "application/json",
            dataType: "json"
        })
    } else {
        renderMessage('Not all mandatory fields are filled in');
    }

}

function filterUser(event: any) {
    const temp: User[] = userList;
    const searchCriteria: string = $('#searchField')
        .val()
        .toString()
        .toLowerCase();

    userList = userList.filter(function (user: User) {
        const name: string = (user.firstname + " " + user.lastname).toLowerCase();
        if (name.search(new RegExp(searchCriteria)) != -1) return user;
    });

    renderUserList();
    userList = temp;
}

function userSort(event: any) {
    userList.sort(function (a: User, b: User): number {
        switch (event.data.name) {
            case "firstname":
                console.log("firstname");
                console.log(a.firstname.localeCompare(b.firstname));
                return a.firstname.localeCompare(b.firstname);
            case "lastname":
                return a.lastname.localeCompare(b.lastname);
            default: 
                return 0;
        }
    });

    if (event.data.order == "desc") userList.reverse();
    renderUserList();
}


function editUser(event) {
    event.preventDefault();

    const editModal: JQuery = $('#edit-user-modal');
    const editUserForm: JQuery = $('#edit-user-form');
    const firstNameInput: JQuery = $('#edit-first-name-input');
    const lastNameInput: JQuery = $('#edit-last-name-input');
    const idHiddenInput: JQuery = $('#edit-id-input');

    const userId: string = idHiddenInput.val().toString();
    const firstName: string = firstNameInput.val().toString().trim();
    const lastName: string = lastNameInput.val().toString().trim();

    if (firstName && lastName) {
        for (const user of userList) {
            if (user.id === userId) {
                $.ajax({
                    url: `${url}/users/${userId}`, 
                    type: "PUT", 
                    data: JSON.stringify({lastname: lastName, firstname: firstName}),
                    success: (success, status, jqXHR) => {
                        const newUser = success as User
                        user.firstname = firstName;
                        user.lastname = lastName;
                        renderMessage(`Successfully updated user ${user.firstname} ${user.lastname}`);
                        editUserForm.trigger('reset');
                        renderUserList();
                        editModal.modal('hide');
                    },
                    contentType: "application/json",
                    dataType: "json"
                })
                return;  // leave function when found
            }
        }
        // The user could not be found, show error message
        renderMessage('The user to update could not be found');
    } else {
        renderMessage('Not all mandatory fields are filled in');
    }
    editModal.modal('hide');
}

function deleteUser(event) {
    const userId: string = $(event.currentTarget).data('user-id');

    $.ajax({
        url: `${url}/users/${userId}`,
        method: "DELETE",
        success: (success, status, xhr) => {
            userList = userList.filter((user) => user.id !== userId);
            renderMessage('User deleted');
            renderUserList();
        }
    })
}

function openEditUserModal(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: string = $(event.currentTarget).data('user-id');

    // Define JQuery HTML objects
    const editUserModal: JQuery = $('#edit-user-modal');
    const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
    const editFirstNameInput: JQuery = $('#edit-first-name-input');
    const editLastNameInput: JQuery = $('#edit-last-name-input');

    // iterate through list until user found (or end of list)
    for (const user of userList) {
        if (user.id === userId) {
            // Fill in edit fields in modal
            editIdInput.val(user.id);
            editFirstNameInput.val(user.firstname);
            editLastNameInput.val(user.lastname);
            // Show modal
            editUserModal.modal('show');
            renderUserList();
            return;  // leave function when found
        }
    }
    renderMessage('The selected user can not be found');
}

/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/
function renderMessage(message: string) {
    // Define JQuery HTML Objects
    const messageWindow: JQuery = $('#messages');

    // Create new alert
    const newAlert: JQuery = $(`
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);

    // Add message to DOM
    messageWindow.append(newAlert);

    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(() => {
        newAlert.alert('close');
    }, 5000);
}

function renderUserList() {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#user-table-body');

    // Delete the old table of users from the DOM
    userTableBody.empty();
    // For each user create a row and append it to the user table
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td>${user.id}</td>
                <td>${user.firstname}</td>
                <td>${user.lastname}</td>
                <td>
                    <button class="btn btn-outline-dark btn-sm edit-user-button mr-4" data-user-id="${user.id}" >
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm delete-user-button mr-4" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);

        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

function triggerModalStat(event) {
    let sumAge: number = 0;
    let currentYear: number = new Date().getFullYear();

    const table: JQuery = $("#statsBody");
    table.empty();

    const content = $(`
        <td scope="row">${4}</td>
    `);
    table.append(content);

    const userStatsModal: JQuery = $('#stats-user-modal');
    userStatsModal.modal('show');
}

/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {
    initUsers();

    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const editUserForm: JQuery = $('#edit-user-form');
    const filterUserForm: JQuery = $('#searchField')
    const userTableBody: JQuery = $('#user-table-body');
    const orderIdDesc: JQuery = $("#orderIdDesc");
    const orderIdAsc: JQuery = $("#orderIdAsc");
    const orderFNameAsc: JQuery = $("#orderFNameAsc");
    const orderFNameDesc: JQuery = $("#orderFNameDesc");
    const orderLNameAsc: JQuery = $("#orderLNameAsc");
    const orderLNameDesc: JQuery = $("#orderLNameDesc");
    const openStatModal: JQuery = $("#openStats");

    // Register listeners
    addUserForm.on('submit', addUser);
    editUserForm.on('submit', editUser);
    filterUserForm.on("keyup", filterUser);
    userTableBody.on('click', '.edit-user-button', openEditUserModal);
    userTableBody.on('click', '.delete-user-button', deleteUser);
    orderIdDesc.on('click', {order: "desc", name: "id"}, userSort);
    orderIdAsc.on('click', {order: "asc", name: "id"}, userSort);
    orderFNameAsc.on('click', {order: "asc", name: "firstname"}, userSort);
    orderFNameDesc.on('click', {order: "desc", name: "firstname"}, userSort);
    orderLNameAsc.on('click', {order: "asc", name: "lastname"}, userSort);
    orderLNameDesc.on('click', {order: "desc", name: "lastname"}, userSort);
    openStatModal.on('click', triggerModalStat);
});
