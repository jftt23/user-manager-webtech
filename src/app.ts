import express from 'express'
import v1 from './routes/api/v1'
import morgan = require('morgan');
import cors = require('cors');
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import { User } from './models/User';

dotenv.config()

const app = express()
const MORGAN_LOGGING_FORMAT: string = ':method :url :status length: :res[content-length] in: :response-time ms'
const CORS_SETTINGS = {
    methods: ['GET', 'POST', 'PUT', 'OPTIONS', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Accept', 'Host', 'User-Agent'],
    origin: 'localhost'
}

// Middleware
app.use(bodyParser.json())
app.use(morgan(MORGAN_LOGGING_FORMAT))
app.use(cors(CORS_SETTINGS))

// V1
app.use('/v1', v1);

app.listen(
    parseInt(process.env.PORT || "5000"),
    process.env.HOST || "localhost",
    () => console.log(`Server started on host: ${process.env.HOST}, listens on port ${process.env.PORT}`)
)

let userList: User[] = [] 

export {app, userList}; // For testing purposes. DO NOT DELETE