import Joi from 'joi'
import { plainToClass } from 'class-transformer'

export const UserSchema = Joi.object({
    id: Joi.string().optional(),
    firstname: Joi.string().required(),
    lastname: Joi.string().required()
})

export class User {
    id: string
    firstname: string
    lastname: string

    constructor(id: string, firstname: string, lastname: string) {
        this.id = id
        this.firstname = firstname
        this.lastname = lastname
    }

    static fromJSON(data: Object): User {
        return plainToClass(User, data)
    }
}
